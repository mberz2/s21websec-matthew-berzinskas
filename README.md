## s21websec-Matthew-Berzinskas

### Introduction

Welcome! This is my Gitlab repository for storing relevant code and notebooks related to **[CS495 Web and Cloud Security Course](https://thefengs.com/wuchang/courses/cs495/)** taught at Portland State University, Spring 2021. 

Lab report notebooks are located in `\notebook\#.pdf`, and the associated code is located in `\lab#\` *(where # in each instance is the relavant lab number, listed below).*

Below you will find a Table of Contents (To-Do List/Completion List), as well as notes related to each update.

### Table of Contents

Lab 1
- [x] Setup
- [x] Web Programming
- [x] Broken Authentication
- [x] Brute Force Vulnerability
- [x] Broken Access Control
- [x] SSRF
- [x] XXE

Lab 2
- [x] Command and SQL injection
- [x] Blind SQL injection

Lab 3
- [x] XSS
- [x] Content Security Policy, CORS
- [x] CSRF
- [x] Clickjacking
- [x] Insecure Deserialization (PHP)
- [x] Insecure Deserialization (JavaScript)

Lab 4
- [x] Thunder CTF
- [x] Thunder CTF / Defender Path
- [x] Serverless Goat
- [x] flaws.cloud
- [x] flaws2.cloud
- [x] CloudGoat

Lab 5
- [x] Tools setup
- [x] wfuzz, nmap, bucket-stream
- [x] wpscan
- [x] hydra, sqlmap, xsstrike, w3af, commix
- [x] metasploit

Final Project
- [x] Excessive trust in client-side controls
- [x] High-level logic vulnerability
- [x] Low-level logic flaw
- [x] Inconsistent handling of exceptional input
- [x] Inconsistent security controls
- [x] Weak isolation on dual-use endpoint
- [x] Insufficient workflow validation
- [x] Authentication bypass via flawed state machine
- [x] Flawed enforcement of business rules
- [x] Infinite money logic flaw
- [x] Authentication bypass via encryption oracle


### Updates
* 3/30/2021 - Initial commit, Python code for HTML/URL crawler `\lab1\1.2`
    * `getUrls_seq.py`, `getUrls_mult.py`, `getUrls_matlib.py`, `getUrls_asynch.py`


* 3/30/2021 - Python code for Broken Authentication `\lab1\1.3`
    * `login.py`, `login2.py`, `login3.py`

* 4/6/2021 - Added (in-progress) Python code for Brute Force Vulnerability HW1 `\lab1\hw1`
    * `hw1.py`

* 4/7/2021 - Added Python code for Broken Access Control `\lab1\1.5`
    * Python script names match PortSwigger levels

* 4/9/2021 - Completed HW1 for Brute Force Vulnerability `\lab1\hw1`
    * `hw1.py` (pages 13-14 of notebook `\notebooks\1.pdf`)

* 4/9/2021 - Added Python code for SSRF `\lab1\1.6`
    * Python script names match PortSwigger levels

* 4/9/2021 - Added Python code for XXE `\lab1\1.7`
    * Python script names match PortSwigger levels

* 4/14/2021 - Beginning of Lab 2 `\lab2\2.1`
    * Python script names match PortSwigger levels

* 4/15/2021 - Continuation of Lab 2 `\lab2\2.1`
    * Python script names match PortSwigger levels

* 4/20/2021 - Completed Lab 2.1 `\lab2\2.1`, started HW2 `\lab2\hw2\`

* 4/22/2021 - Completed HW2 `\lab2\hw2\`
    * `hw2.py` (pages 19-20 of notebook `\notebooks\2.pdf`)
    * See program header comments for installation, and syntax.

* 4/26/2021 - Initiated Lab 3 `\lab3\`

* 4/28/2021 - Progress on 3.1 and 3.2 `\lab3\3.1` and `\lab3\3.2`.

* 4/29/2021 - Completed 3.1, 3.2, 3.3 in `\lab3\3.*`

* 5/4/2021 - Completed 3.4, 3.5, 3.6 in `\lab3\3.*`

* 5/12/2021 - Completed 4.1 and 4.2 (Notebook Only)

* 5/20/2021 - Completeed 4.3 and 4.4 (Notebook Only)

* 5/25/2021 - Complete Notebook 4, started Notebook 5.

* 6/2/2021 - Complete Notebook 5.

* 6/7/2021 - Began fianl project work. Consisting of PortSwigger levels in "Business Logic Section".

* 6/10/2021 - Final Project Complete. Viewable at URL in `/final/url.txt`

## Disclaimer

This repository contains beginner level code and coursework related to specific objectives and assignments as prescribed by a higher education institution. While it main contain tools for compromising web and clouds systems, the techniques and tools described in this repo are not intended for use against any site outside of the course sites. The contained CTFs are for practicing security concepts without breaking the law.

💻