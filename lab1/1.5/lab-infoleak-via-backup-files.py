import requests
from bs4 import BeautifulSoup

s = requests.Session()

site = 'acac1f3b1e6597c380fc0295003e0058.web-security-academy.net'
url = f'https://{site}'

resp = s.get(f'{url}/backup/ProductTemplate.java.bak')
soup = BeautifulSoup(resp.text,'html.parser') 
print(soup)