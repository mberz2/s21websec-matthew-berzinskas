import requests
from bs4 import BeautifulSoup

s = requests.Session()

site = 'ac941f491fa656f08062097e00a80050.web-security-academy.net'
url = f'https://{site}'

resp = s.get(url)
soup = BeautifulSoup(resp.text,'html.parser') 
print(soup)

resp = s.get(f'{url}/cgi-bin/phpinfo.php')
soup = BeautifulSoup(resp.text,'html.parser') 
print(soup)